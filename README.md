# pinage404/work-toolbox

[![Pipeline status][pipeline-badge]][pipeline-url]
[![Docker stars][docker-badge-stars]][docker-repository-url]
[![Docker pulls][docker-badge-pulls]][docker-repository-url]
[![MIT License][license-badge]][license-url]

[Docker][docker] image made to have a working environment without installing anything other than Docker

## Docker Hub Repository

[Docker Hub Repository][docker-repository-url]

## GitLab Repository

[Git Repository][git-repository-url]

## How to use?

### Docker Compose

Use the [Docker Compose][docker-compose-url] file to use it simple

[Docker Compose File][docker-compose-file-url]

```sh
git clone git@gitlab.com:pinage404/work-toolbox.git
cd work-toolbox
# or
curl -sSl https://gitlab.com/pinage404/work-toolbox/raw/master/docker-compose.yml

# then
docker-compose run shell
```

### Docker CLI

```sh
docker run -it --rm -v ~/Project:/project pinage404/work-toolbox
```

## What is in the box?

See the [Dockerfile][dockerfile-url]

It includes:

- Git
- Git Flow
- Fish
- Oh My Fish
- The Silver Searcher (`ag`)
- `sed`
- PlantUML
- ngrok

## License

[MIT License][license-url]



[pipeline-badge]: https://gitlab.com/pinage404/work-toolbox/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/pinage404/work-toolbox/commits/master

[docker-badge-stars]: https://img.shields.io/docker/stars/pinage404/work-toolbox.svg
[docker-badge-pulls]: https://img.shields.io/docker/pulls/pinage404/work-toolbox.svg
[docker-repository-url]: https://hub.docker.com/r/pinage404/work-toolbox/

[license-badge]: https://img.shields.io/badge/license-MIT-green.svg
[license-url]: https://gitlab.com/pinage404/work-toolbox/blob/master/LICENSE

[docker]: https://www.docker.com

[git-repository-url]: https://gitlab.com/pinage404/work-toolbox

[docker-compose-url]: https://docs.docker.com/compose/
[docker-compose-file-url]: https://gitlab.com/pinage404/work-toolbox/blob/master/docker-compose.yml

[dockerfile-url]: https://gitlab.com/pinage404/work-toolbox/blob/master/Dockerfile
