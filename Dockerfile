FROM ubuntu:latest


WORKDIR /project
VOLUME [ "/project" ]


# install pre required packages
RUN apt-get -qq update -y
RUN apt-get -qq install -y \
	# add APT repo
	add-apt-key software-properties-common apt-utils sudo \
	# remote
	curl wget \
	# security
	lsb lsb-release \
	gnupg gnupg2 \
	apt-transport-https \
	debian-keyring ca-certificates \
&& echo ""


# set locales to french
RUN locale-gen fr_FR.UTF-8
ENV LANG       fr_FR.UTF-8
ENV LANGUAGE   fr_FR:fr
ENV LC_ALL     fr_FR.UTF-8


# add Fish repo
RUN apt-add-repository -y ppa:fish-shell/release-2


# install packages
RUN apt-get -qq update -y
RUN apt-get -qq install -y \
	# shell
	fish \
	bash bash-completion \
	# archive
	tar unrar zip unzip bzip2 p7zip-full \
	# file view
	less most \
	wdiff \
	# tree view
	tree \
	# search
	silversearcher-ag \
	# replace
	sed \
	# build tools
	build-essential \
	# Git
	git git-flow git-remote-gcrypt git-extras \
	# Python
	python3 python3-dev python3-pip \
	python python-dev python-pip \
	# editor
	nano vim \
	editorconfig \
	# PlantUML
	graphviz plantuml \
	# network
	mtr \
	# helpers
	command-not-found thefuck \
&& echo ""


# Nano syntax highlight
ADD --chown=1000:1000 https://raw.githubusercontent.com/scopatz/nanorc/master/install.sh /tmp/nanorc.sh
RUN bash /tmp/nanorc.sh
RUN rm /tmp/nanorc.sh


# RipGrep (`rg`) (like the silver searcher (`ag`) but even more faster)
ADD --chown=1000:1000 https://github.com/BurntSushi/ripgrep/releases/download/0.8.0/ripgrep_0.8.0_amd64.deb /tmp/ripgrep.deb
RUN dpkg --install /tmp/ripgrep.deb
RUN rm /tmp/ripgrep.deb


# install ngrok
ADD --chown=1000:1000 https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip /tmp/ngrok.zip
RUN unzip "/tmp/ngrok.zip" -d "/usr/bin/"
RUN rm "/tmp/ngrok.zip"
EXPOSE 4040


# add non-root user
ARG USER=dev
RUN useradd --create-home --shell $(which fish) --user-group ${USER}
USER ${USER}


# set default shell to Fish
SHELL [ "fish", "--command" ]
CMD [ "fish" ]


# install Oh My Fish
COPY --chown=1000:1000 .config/omf/* /home/${USER}/.config/omf/
RUN echo "set -g default_user $USER" >> /home/$USER/.config/omf/init.fish
ADD --chown=1000:1000 https://get.oh-my.fish /tmp/omf_install.fish
RUN fish /tmp/omf_install.fish --noninteractive
RUN rm /tmp/omf_install.fish
RUN omf install
RUN omf update
